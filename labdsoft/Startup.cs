﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(labdsoft.Startup))]
namespace labdsoft
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
