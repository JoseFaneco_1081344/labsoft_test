﻿using labdsoft.Controllers;
using labdsoft.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace labdsoft.Tests
{
    [TestFixture]
    public class TestMatch
    {

        [Test]
        public void testMatch()
        {

            Receiver receiver = new Receiver();
            receiver.IDPerson = 1;
            receiver.Name = "Claudio";
            receiver.InDate = DateTime.Now;
            receiver.adress = "Viana do Castelo";
            receiver.BornDate = DateTime.Now;
            receiver.cc = 150121;
            receiver.Nationality = "Por";
            receiver.IMC = 20;

            HLA hla = new HLA();
            hla.IDPerson = 1;
            hla.TypeA = 1;
            hla.TypeA = 2;
            hla.TypeC = 3;
            hla.TypeDQ = 4;
            hla.TypeDR = 5;

            hla.A = 1;
            hla.B = 2;
            hla.C = 3;
            hla.DQ = 4;
            hla.DR = 5;

            ReceiverState receiverState = new ReceiverState();
            receiverState.IDPerson = 1;
            receiverState.state = ReceiverStatePo.U2;

            OrganType organType = new OrganType();
            organType.OrganTypeId = 1;
            organType.Description = "Rins";

            OrganNeeded organNeeded = new OrganNeeded();
            organNeeded.IDPerson = 1;
            organNeeded.IDOrgan = organType.OrganTypeId;

            Receiver receiver_2 = new Receiver();
            receiver_2.IDPerson = 2;
            receiver_2.Name = "Diogo";
            receiver_2.InDate = DateTime.Now;
            receiver_2.adress = "Viana do Castelo";
            receiver_2.BornDate = DateTime.Now;
            receiver_2.cc = 150121;
            receiver_2.Nationality = "Por";
            receiver_2.IMC = 80;

            HLA hla_2 = new HLA();
            hla_2.IDPerson = 2;
            hla_2.TypeA = 1;
            hla_2.TypeA = 2;
            hla_2.TypeC = 3;
            hla_2.TypeDQ = 4;
            hla_2.TypeDR = 5;

            hla_2.A = 1;
            hla_2.B = 2;
            hla_2.C = 3;
            hla_2.DQ = 4;
            hla_2.DR = 5;

            ReceiverState receiverState_2 = new ReceiverState();
            receiverState_2.IDPerson = 2;
            receiverState_2.state = ReceiverStatePo.SU;

            OrganNeeded organNeeded_2 = new OrganNeeded();
            organNeeded_2.IDPerson = 1;
            organNeeded_2.IDOrgan = organType.OrganTypeId;


            Donor donor = new Donor();
            donor.IDPerson = 3;
            donor.Name = "Mario";
            donor.DeadDate = DateTime.Now;
            donor.CollectionDate = DateTime.Now;
            donor.Category = 2;
            donor.Observations = "Observations";

            OrganDonated organDonated = new OrganDonated();
            organDonated.OrganDonatedId = 1;
            organDonated.OrganID = 1;
            organDonated.IDPerson = donor.IDPerson;

            HLA hla_3 = new HLA();
            hla_3.IDPerson = 3;
            hla_3.TypeA = 1;
            hla_3.TypeA = 2;
            hla_3.TypeC = 3;
            hla_3.TypeDQ = 4;
            hla_3.TypeDR = 5;

            hla_3.A = 1;
            hla_3.B = 2;
            hla_3.C = 3;
            hla_3.DQ = 4;
            hla_3.DR = 5;

            MatchesController controller = new MatchesController();

            //Dictionary<Receiver, double> result = controller.FindMatch(organDonated.OrganDonatedId);

            Assert.AreEqual(true, true);
        }

        [Test]
        public void testBloodChecker()
        {
            MatchesController controller = new MatchesController();

            Assert.AreEqual(true, controller.BloodChecker("0-", "O+"));
            Assert.AreNotEqual(false, controller.BloodChecker("B-", "B+"));
            Assert.AreEqual(true, controller.BloodChecker("AB+", "B+"));
            Assert.AreEqual(true, controller.BloodChecker("AB+", "B-"));

        }
        
        [Test]
        public void testHLACompatibility()
        {

            HLA hla = new HLA();
            hla.IDPerson = 1;
            hla.TypeA = 1;
            hla.TypeA = 2;
            hla.TypeC = 3;
            hla.TypeDQ = 4;
            hla.TypeDR = 5;

            hla.A = 1;
            hla.B = 2;
            hla.C = 3;
            hla.DQ = 4;
            hla.DR = 5;

            HLA hla_2 = new HLA();
            hla_2.IDPerson = 2;
            hla_2.TypeA = 1;
            hla_2.TypeA = 2;
            hla_2.TypeC = 3;
            hla_2.TypeDQ = 3;
            hla_2.TypeDR = 3;

            hla_2.A = 1;
            hla_2.B = 2;
            hla_2.C = 3;
            hla_2.DQ = 3;
            hla_2.DR = 3;

            HLA hla_3 = new HLA();
            hla_3.IDPerson = 3;
            hla_3.TypeA = 1;
            hla_3.TypeA = 2;
            hla_3.TypeC = 3;
            hla_3.TypeDQ = 4;
            hla_3.TypeDR = 5;

            hla_3.A = 1;
            hla_3.B = 2;
            hla_3.C = 3;
            hla_3.DQ = 4;
            hla_3.DR = 5;

            int expectedResult_1 = 12;

            int expectedResult_2 = 4;

            MatchesController controller = new MatchesController();

            int result_1 = controller.HLACompatibility(hla, hla_3);

            int result_2 = controller.HLACompatibility(hla_2, hla_3);
            
            Assert.AreEqual(expectedResult_1, result_1);
            Assert.AreEqual(expectedResult_2, result_2);
        }


        [Test]
        public void testWaitTimeStartAnalysis()
        {

            Receiver receiver = new Receiver();
            receiver.IDPerson = 5;
            receiver.Name = "Claudio";
            receiver.InDate = DateTime.Now;
            receiver.adress = "Viana do Castelo";
            receiver.BornDate = DateTime.Now;
            receiver.cc = 150121;
            receiver.Nationality = "Por";
            receiver.IMC = 20;

            MatchesController controller = new MatchesController();

            Double result = controller.WaitTimeStartAnalysis(receiver);


            Assert.AreEqual(result, result);

        }

        [Test]
        public void testGetRankCurrentDate()
        {

            Receiver receiver = new Receiver();
            receiver.IDPerson = 5;
            receiver.Name = "Claudio";
            receiver.InDate = DateTime.Now;
            receiver.adress = "Viana do Castelo";
            receiver.BornDate = DateTime.Now;
            receiver.cc = 150121;
            receiver.Nationality = "Por";
            receiver.IMC = 20;

            int expectedResult = 5;

            MatchesController controller = new MatchesController();

            int result_1 = controller.GetRankCurrentDate(receiver);

            Assert.AreEqual(expectedResult, result_1);
        }
    }
}