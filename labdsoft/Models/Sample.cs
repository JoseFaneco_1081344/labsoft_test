﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace labdsoft.Models
{
    public class Sample
    {
        public int sampleId { get; set; }

        [ForeignKey("Person")]
        public int IDPerson { get; set; }
        public virtual Person Person { get; set; }
    }
}