﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace labdsoft.Models
{
    public class AnalysisRequest
    {
        [Key, Column(Order = 0)]        
        [ForeignKey("IDAnalysis")]
        [Display(Name = "Type of Analysis")]
        public int IDTypeAnalysis { get; set; }
        public virtual Analysis IDAnalysis { get; set; }


        [Key, Column(Order = 1)]        
        [ForeignKey("Sample")]
        [Display(Name = "Sample identifier")]
        public int IDSample { get; set; }
        public virtual Sample Sample { get; set; }
    }
}