﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace labdsoft.Models
{
    public class HLA
    {

        [Key, ForeignKey("Person")]
        public int IDPerson { get; set; }

        public virtual Person Person { get; set; }

        public int TypeA { get; set; }
        public int TypeB { get; set; }
        public int TypeC { get; set; }
        public int TypeDR { get; set; }
        public int TypeDQ { get; set; }

        public double A { get; set; }
        public double B { get; set; }
        public double C { get; set; }
        public double DR { get; set; }
        public double DQ { get; set; }


    }
}