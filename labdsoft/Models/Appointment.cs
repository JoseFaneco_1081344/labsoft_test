﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace labdsoft.Models
{
    public class Appointment
    {
        public int AppointmentId { get; set; }

        [ForeignKey("Person")]
        public int IDPerson { get; set; }

        public virtual Person Person { get; set; }

        [Required]
        public string WorkerID { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        public string Observations { get; set; }

    }
}