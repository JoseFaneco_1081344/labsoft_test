﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace labdsoft.Models
{
    public class OrganNeeded
    {
        [Key, Column(Order = 0)]
        [ForeignKey("Person")]
        public int IDPerson { get; set; }

        public virtual Person Person { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("OrganType")]
        public int IDOrgan { get; set; }

        public virtual OrganType OrganType { get; set; }
    }
}