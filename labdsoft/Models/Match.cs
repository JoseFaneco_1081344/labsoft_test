﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace labdsoft.Models
{
    public class Match
    {
        [Key]
        public int IdMatch { get; set; }

        [ForeignKey("OrganDonated")]
        public int OrganDonatedId { get; set; }
        public virtual OrganDonated OrganDonated { get; set; }
    
    }
}