﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace labdsoft.Models
{
    public class CITO
    {

        [Required]
        [Key]
        public int IDCito { get; set; }

        [Required]
        public string Local { get; set; }

        //In Minutes
        [Display(Name = "Organ maximum life time (Minutes)")]
        public int OrganLifeTime { get; set; } = 120;

    }
}