﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace labdsoft.Models
{
    public class ReceiverMatch
    {
  
        [Key]
        public int IdReceiverMatch { get; set; }

        [ForeignKey("Person")]
        public int? PersonId { get; set; }
        public virtual Person Person { get; set; }

        [ForeignKey("Match")]
        public int? MatchId { get; set; }
        public virtual Match Match { get; set; }

        public double Rank { get; set; }

    }
}