﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;




namespace labdsoft.Models
{
    public enum ReceiverStatePo
    {
        [Display(Name = "CD")]
        CD = 3,
        [Display(Name = "CT")]
        CT = 2,
        [Display(Name = "SU")]
        SU = 1,
        [Display(Name = "U2")]
        U2 = 0
    }

    public class ReceiverState
    {
        [Key]
        [ForeignKey("Person")]
        public int IDPerson { get; set; }
        public virtual Person Person { get; set; }


        [Display(Name = "User State")]
        public ReceiverStatePo state { get; set; }

    }
}