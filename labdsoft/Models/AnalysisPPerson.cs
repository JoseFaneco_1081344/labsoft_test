﻿using labdsoft.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace labdsoft.Models
{
    public enum State
    {
        [Display(Name = "Confirmed")]
        confirmed = 1,
        [Display(Name = "Not confirmed")]
        nConfirmed = 0
    }

    public class AnalysisPPerson
    {
        [Key, Column(Order = 0)]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("Person")]
        [Display(Name = "Person Name")]
        public int IDPerson { get; set; }

        [ForeignKey("IDAnalysis")]
        [Display(Name = " Type of analysis")]
        public int TypeAnalysis { get; set; }
        public virtual Analysis IDAnalysis { get; set; }

        public double Result { get; set; }

        public State state { get; set; }

        public virtual Person Person { get; set; }


    }
}