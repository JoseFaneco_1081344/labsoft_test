﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace labdsoft.Models
{
    public class ReceiverTracker
    {
        [Key, Column(Order = 0)]
        public string TrackerID { get; set; }

        [ForeignKey("Person")]
        public int IDPerson { get; set; }
        public virtual Person Person { get; set; }
    }
}