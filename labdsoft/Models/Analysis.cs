﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using labdsoft.Models;

namespace labdsoft.Models
{
    public class Analysis
    {
        [Key]
        [Required]
        public int IDAnalysis { get; set; }

        [ForeignKey("IDCito")]
        public int CitoID { get; set; }

        public virtual CITO IDCito { get; set; }

        [StringLength(50)]
        [Display(Name = "Analysis description")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Minimum value for this analysis")]
        public double MinValue { get; set; }

        [Required]
        [Display(Name = "Maximum value for this analysis")]
        public double MaxValue { get; set; }
    }
}