﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace labdsoft.Models
{
    public abstract class Person
    {

        [Key]
        public int IDPerson { get; set; }


        [Required]
        [Display(Name = "Worker name")]
        public string WorkerID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [ForeignKey("BloodType")]
        [Display(Name = "Blood Type")]
        public int BloodtypeID { get; set; }

        public virtual BloodType BloodType { get; set; }
    }
}