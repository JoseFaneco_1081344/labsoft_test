﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace labdsoft.Models
{
    public class SampleLocal
    {
        [Key, Column(Order =0)]
        [ForeignKey("SampleId")]
        public int IDSample { get; set; }
        public virtual Sample SampleId { get; set; }

        [Key, Column(Order =1)]
        [ForeignKey("LocalId")]
        public int IDLocal { get; set; }
        public virtual Local LocalId { get; set; }

        public override string ToString()
        {
            return "IDSample: "+ IDSample + ", Local Desc: "+LocalId.Description;
        }
    }

   

}