﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace labdsoft.Models
{
    public class OrganDonated
    {
        public int OrganDonatedId { get; set; }

        [ForeignKey("OrganType")]
        public int OrganID { get; set; }
        public virtual OrganType OrganType { get; set; }

        [ForeignKey("Person")]
        public int IDPerson { get; set; }
        public virtual Person Person { get; set; }
    }
}