﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace labdsoft.Models
{
    public class OrganType
    {
        public int OrganTypeId { get; set; }

        [Required]
        public String Description { get; set; }


    }
}