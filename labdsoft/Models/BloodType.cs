﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace labdsoft.Models
{
    public class BloodType
    {
        public int BloodTypeID { get; set; }

        [Required]
        public string Description { get; set; }
    }
}