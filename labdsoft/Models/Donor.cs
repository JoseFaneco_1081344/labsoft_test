﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace labdsoft.Models
{
    public class Donor : Person
    {

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Obit Date")]
        public DateTime DeadDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Collection Date")]
        public DateTime CollectionDate { get; set; }

        [Range(0, 5)]
        public int Category { get; set; }

        public string Observations { get; set; }


    }
}