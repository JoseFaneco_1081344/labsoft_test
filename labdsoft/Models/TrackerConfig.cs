﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace labdsoft.Models
{
    public class TrackerConfig
    {
        [Key]
        [ForeignKey("TrackerID")]
        public string IDTracker { get; set; }
        public virtual ReceiverTracker TrackerID { get; set; }

        [Display(Name = "Min Blood Pressure")]
        public double min_TA { get; set; }
        [Display(Name = "Max Blood Pressure")]
        public double max_TA { get; set; }

        [Display(Name = "Min Body Temperature")]
        public double min_Temp { get; set; }
        [Display(Name = "Max Body Temperature")]
        public double max_Temp { get; set; }

        [Display(Name = "Min Heart Rate")]
        public int min_HR { get; set; }
        [Display(Name = "Max Heart Rate")]
        public int max_HR { get; set; }


    }
}