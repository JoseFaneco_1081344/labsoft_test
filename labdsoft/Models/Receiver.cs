﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace labdsoft.Models
{
    public class Receiver : Person
    {
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Register Date")]
        public DateTime InDate { get; set; }


        [Required]
        [Display(Name = "Address")]
        public string adress { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime BornDate { get; set; }

        [Display(Name = "Social Number")]
        public int cc { get; set; }

        public string Nationality { get; set; }

        [Range(0, 100.0)]
        [Display(Name = "Body fat in %")]
        public double IMC { get; set; }
    }
}