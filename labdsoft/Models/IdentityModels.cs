﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace labdsoft.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<labdsoft.Models.Analysis> Analyses { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.CITO> CITOes { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.AnalysisPPerson> AnalysisPPersons { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.Person> People { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.Appointment> Appointments { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.BloodType> BloodTypes { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.Donor> Donors { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.HLA> HLAs { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.Local> Locals { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.OrganType> OrganTypes { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.Receiver> Receivers { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.OrganNeeded> OrganNeededs { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.OrganDonated> OrganDonateds { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.WasteOrganReg> WasteOrganRegs { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.Sample> Samples { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.SampleLocal> SampleLocals { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.ReceiverState> ReceiverStates { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.AnalysisRequest> AnalysisRequests { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.ReceiverTracker> ReceiverTrackers { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.TrackerConfig> TrackerConfigs { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.Match> Matches { get; set; }

        public System.Data.Entity.DbSet<labdsoft.Models.ReceiverMatch> ReceiverMatches { get; set; }
    }
}