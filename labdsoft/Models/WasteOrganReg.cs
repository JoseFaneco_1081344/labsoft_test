﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace labdsoft.Models
{
    public class WasteOrganReg
    {
        [Key]
        [ForeignKey("IDOrgan")]
        public int OrganID { get; set; }
        public virtual OrganDonated IDOrgan { get; set; }

        [Required]
        public string Reason { get; set; }
    }
}