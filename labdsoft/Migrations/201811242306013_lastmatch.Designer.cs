// <auto-generated />
namespace labdsoft.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class lastmatch : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(lastmatch));
        
        string IMigrationMetadata.Id
        {
            get { return "201811242306013_lastmatch"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
