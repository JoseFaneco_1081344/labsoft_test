namespace labdsoft.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Analyses",
                c => new
                    {
                        IDAnalysis = c.Int(nullable: false, identity: true),
                        CitoID = c.Int(nullable: false),
                        Description = c.String(maxLength: 50),
                        MinValue = c.Double(nullable: false),
                        MaxValue = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.IDAnalysis)
                .ForeignKey("dbo.CITOes", t => t.CitoID, cascadeDelete: true)
                .Index(t => t.CitoID);
            
            CreateTable(
                "dbo.CITOes",
                c => new
                    {
                        IDCito = c.Int(nullable: false, identity: true),
                        Local = c.String(nullable: false),
                        OrganLifeTime = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IDCito);
            
            CreateTable(
                "dbo.AnalysisPPersons",
                c => new
                    {
                        Date = c.DateTime(nullable: false),
                        IDPerson = c.Int(nullable: false),
                        TypeAnalysis = c.Int(nullable: false),
                        Result = c.Double(nullable: false),
                        state = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Date, t.IDPerson })
                .ForeignKey("dbo.Analyses", t => t.TypeAnalysis, cascadeDelete: true)
                .ForeignKey("dbo.People", t => t.IDPerson, cascadeDelete: true)
                .Index(t => t.IDPerson)
                .Index(t => t.TypeAnalysis);
            
            CreateTable(
                "dbo.People",
                c => new
                    {
                        IDPerson = c.Int(nullable: false, identity: true),
                        WorkerID = c.String(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        BloodtypeID = c.Int(nullable: false),
                        DeadDate = c.DateTime(),
                        CollectionDate = c.DateTime(),
                        Category = c.Int(),
                        Observations = c.String(),
                        InDate = c.DateTime(),
                        adress = c.String(),
                        BornDate = c.DateTime(),
                        cc = c.Int(),
                        Nationality = c.String(),
                        IMC = c.Double(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.IDPerson)
                .ForeignKey("dbo.BloodTypes", t => t.BloodtypeID, cascadeDelete: true)
                .Index(t => t.BloodtypeID);
            
            CreateTable(
                "dbo.BloodTypes",
                c => new
                    {
                        BloodTypeID = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.BloodTypeID);
            
            CreateTable(
                "dbo.Appointments",
                c => new
                    {
                        AppointmentId = c.Int(nullable: false, identity: true),
                        IDPerson = c.Int(nullable: false),
                        WorkerID = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Observations = c.String(),
                    })
                .PrimaryKey(t => t.AppointmentId)
                .ForeignKey("dbo.People", t => t.IDPerson, cascadeDelete: true)
                .Index(t => t.IDPerson);
            
            CreateTable(
                "dbo.HLAs",
                c => new
                    {
                        IDPerson = c.Int(nullable: false),
                        A = c.Double(nullable: false),
                        B = c.Double(nullable: false),
                        C = c.Double(nullable: false),
                        DR = c.Double(nullable: false),
                        DQ = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.IDPerson)
                .ForeignKey("dbo.People", t => t.IDPerson)
                .Index(t => t.IDPerson);
            
            CreateTable(
                "dbo.Locals",
                c => new
                    {
                        LocalID = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.LocalID);
            
            CreateTable(
                "dbo.OrganTypes",
                c => new
                    {
                        OrganTypeId = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.OrganTypeId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.HLAs", "IDPerson", "dbo.People");
            DropForeignKey("dbo.Appointments", "IDPerson", "dbo.People");
            DropForeignKey("dbo.AnalysisPPersons", "IDPerson", "dbo.People");
            DropForeignKey("dbo.People", "BloodtypeID", "dbo.BloodTypes");
            DropForeignKey("dbo.AnalysisPPersons", "TypeAnalysis", "dbo.Analyses");
            DropForeignKey("dbo.Analyses", "CitoID", "dbo.CITOes");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.HLAs", new[] { "IDPerson" });
            DropIndex("dbo.Appointments", new[] { "IDPerson" });
            DropIndex("dbo.People", new[] { "BloodtypeID" });
            DropIndex("dbo.AnalysisPPersons", new[] { "TypeAnalysis" });
            DropIndex("dbo.AnalysisPPersons", new[] { "IDPerson" });
            DropIndex("dbo.Analyses", new[] { "CitoID" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.OrganTypes");
            DropTable("dbo.Locals");
            DropTable("dbo.HLAs");
            DropTable("dbo.Appointments");
            DropTable("dbo.BloodTypes");
            DropTable("dbo.People");
            DropTable("dbo.AnalysisPPersons");
            DropTable("dbo.CITOes");
            DropTable("dbo.Analyses");
        }
    }
}
