namespace labdsoft.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tracker : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ReceiverTrackers",
                c => new
                    {
                        TrackerID = c.String(nullable: false, maxLength: 128),
                        IDPerson = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TrackerID)
                .ForeignKey("dbo.People", t => t.IDPerson, cascadeDelete: true)
                .Index(t => t.IDPerson);
            
            CreateTable(
                "dbo.TrackerConfigs",
                c => new
                    {
                        IDTracker = c.String(nullable: false, maxLength: 128),
                        min_TA = c.Double(nullable: false),
                        max_TA = c.Double(nullable: false),
                        min_Temp = c.Double(nullable: false),
                        max_Temp = c.Double(nullable: false),
                        min_HR = c.Int(nullable: false),
                        max_HR = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IDTracker)
                .ForeignKey("dbo.ReceiverTrackers", t => t.IDTracker)
                .Index(t => t.IDTracker);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TrackerConfigs", "IDTracker", "dbo.ReceiverTrackers");
            DropForeignKey("dbo.ReceiverTrackers", "IDPerson", "dbo.People");
            DropIndex("dbo.TrackerConfigs", new[] { "IDTracker" });
            DropIndex("dbo.ReceiverTrackers", new[] { "IDPerson" });
            DropTable("dbo.TrackerConfigs");
            DropTable("dbo.ReceiverTrackers");
        }
    }
}
