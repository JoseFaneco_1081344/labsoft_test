namespace labdsoft.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class hla : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.HLAs", "IDPerson", "dbo.People");
            DropPrimaryKey("dbo.HLAs");
            AddColumn("dbo.HLAs", "TypeA", c => c.Int(nullable: false));
            AddColumn("dbo.HLAs", "TypeB", c => c.Int(nullable: false));
            AddColumn("dbo.HLAs", "TypeC", c => c.Int(nullable: false));
            AddColumn("dbo.HLAs", "TypeDR", c => c.Int(nullable: false));
            AddColumn("dbo.HLAs", "TypeDQ", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.HLAs", "IDPerson");
            AddForeignKey("dbo.HLAs", "IDPerson", "dbo.People", "IDPerson");
            DropColumn("dbo.HLAs", "IdHla");
        }
        
        public override void Down()
        {
            AddColumn("dbo.HLAs", "IdHla", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.HLAs", "IDPerson", "dbo.People");
            DropPrimaryKey("dbo.HLAs");
            DropColumn("dbo.HLAs", "TypeDQ");
            DropColumn("dbo.HLAs", "TypeDR");
            DropColumn("dbo.HLAs", "TypeC");
            DropColumn("dbo.HLAs", "TypeB");
            DropColumn("dbo.HLAs", "TypeA");
            AddPrimaryKey("dbo.HLAs", "IdHla");
            AddForeignKey("dbo.HLAs", "IDPerson", "dbo.People", "IDPerson", cascadeDelete: true);
        }
    }
}
