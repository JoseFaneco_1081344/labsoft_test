namespace labdsoft.Migrations
{
    using labdsoft.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<labdsoft.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(labdsoft.Models.ApplicationDbContext context)
        {

            var store = new RoleStore<IdentityRole>(context);
            var manager = new RoleManager<IdentityRole>(store);

            string[] roleNames = { "Doctor", "Nurse", "LabTec", "LabDir", "PickTec" };

            IdentityResult roleResult;
            foreach (var roleName in roleNames)
            {
                var roleExist = manager.RoleExists(roleName);
                if (!roleExist)
                {
                    roleResult = manager.Create(new IdentityRole(roleName));
                }
            }

            if (!context.Users.Any(u => u.UserName == "doctor@cito.pt"))
            {
                var storeU = new UserStore<ApplicationUser>(context);
                var managerU = new UserManager<ApplicationUser>(storeU);
                var user = new ApplicationUser { UserName = "doctor@cito.pt", Email = "doctor@cito.pt", LockoutEnabled = true };
                var a = managerU.Create(user, "Citos1234!");
                managerU.AddToRole(user.Id, "Doctor");
            }

            if (!context.Users.Any(u => u.UserName == "nurse@cito.pt"))
            {
                var storeU = new UserStore<ApplicationUser>(context);
                var managerU = new UserManager<ApplicationUser>(storeU);
                var user = new ApplicationUser { UserName = "nurse@cito.pt", Email = "nurse@cito.pt", LockoutEnabled = true };
                var a = managerU.Create(user, "Citos1234!");
                managerU.AddToRole(user.Id, "Nurse");
            }

            if (!context.Users.Any(u => u.UserName == "labtec@cito.pt"))
            {
                var storeU = new UserStore<ApplicationUser>(context);
                var managerU = new UserManager<ApplicationUser>(storeU);
                var user = new ApplicationUser { UserName = "labtec@cito.pt", Email = "labtec@cito.pt", LockoutEnabled = true };
                var a = managerU.Create(user, "Citos1234!");
                managerU.AddToRole(user.Id, "LabTec");
            }

            if (!context.Users.Any(u => u.UserName == "labdir@cito.pt"))
            {
                var storeU = new UserStore<ApplicationUser>(context);
                var managerU = new UserManager<ApplicationUser>(storeU);
                var user = new ApplicationUser { UserName = "labdir@cito.pt", Email = "labdir@cito.pt", LockoutEnabled = true };
                var a = managerU.Create(user, "Citos1234!");
                managerU.AddToRole(user.Id, "LabDir");
            }

            if (!context.Users.Any(u => u.UserName == "picktec@cito.pt"))
            {
                var storeU = new UserStore<ApplicationUser>(context);
                var managerU = new UserManager<ApplicationUser>(storeU);
                var user = new ApplicationUser { UserName = "picktec@cito.pt", Email = "picktec@cito.pt", LockoutEnabled = true };
                var a = managerU.Create(user, "Citos1234!");
                managerU.AddToRole(user.Id, "PickTec");
            }

            //-------------------------------------------------------
            //--------------- Organ Types ---------------------------
            if (!context.Set<OrganType>().Any())
            {
                OrganType Rins = new OrganType();
                Rins.Description = "Rins";
                context.Set<OrganType>().AddOrUpdate(Rins);

                OrganType coracao = new OrganType();
                coracao.Description = "Cora��o";
                context.Set<OrganType>().AddOrUpdate(coracao);

                OrganType C�rnea = new OrganType();
                C�rnea.Description = "C�rnea";
                context.Set<OrganType>().AddOrUpdate(C�rnea);

                OrganType F�gado = new OrganType();
                F�gado.Description = "Figado";
                context.Set<OrganType>().AddOrUpdate(F�gado);

                OrganType Intestino = new OrganType();
                Intestino.Description = "Intestino ";
                context.Set<OrganType>().AddOrUpdate(Intestino);

                OrganType P�ncreas = new OrganType();
                P�ncreas.Description = "P�ncreas";
                context.Set<OrganType>().AddOrUpdate(P�ncreas);

                OrganType Pulm�o = new OrganType();
                Pulm�o.Description = "Pulm�o";
                context.Set<OrganType>().AddOrUpdate(Pulm�o);
            }

            //-------------------------------------------------
            //-------------Blood types ------------------------
            if (!context.Set<BloodType>().Any())
            {
                BloodType aplus = new BloodType();
                aplus.Description = "A+";
                context.Set<BloodType>().AddOrUpdate(aplus);

                BloodType oplus = new BloodType();
                oplus.Description = "O+";
                context.Set<BloodType>().AddOrUpdate(oplus);

                BloodType bplus = new BloodType();
                bplus.Description = "B+";
                context.Set<BloodType>().AddOrUpdate(bplus);

                BloodType abplus = new BloodType();
                abplus.Description = "AB+";
                context.Set<BloodType>().AddOrUpdate(abplus);

                BloodType aminus = new BloodType();
                aminus.Description = "A-";
                context.Set<BloodType>().AddOrUpdate(aminus);

                BloodType ominus = new BloodType();
                ominus.Description = "O-";
                context.Set<BloodType>().AddOrUpdate(ominus);

                BloodType bminus = new BloodType();
                bminus.Description = "B-";
                context.Set<BloodType>().AddOrUpdate(bminus);

                BloodType abminus = new BloodType();
                abminus.Description = "AB-";
                context.Set<BloodType>().AddOrUpdate(abminus);
            }
            //---------------------------------------------
            //-------------- Analysis ---------------------
            if (!context.Set<Analysis>().Any())
            {
                CITO cito = new CITO();
                cito.Local = "Norte";
                cito.OrganLifeTime = 130;
                if (!context.Set<CITO>().Any(e => e.Local == cito.Local))
                {
                    context.Set<CITO>().Add(cito);
                }
                CITO fk = context.Set<CITO>().SingleOrDefault(a => a.Local == "Norte");

                Analysis Hb = new Analysis();
                Hb.CitoID = fk.IDCito;
                Hb.Description = "HBsAg - CMIA";
                Hb.MinValue = 1.0;
                Hb.MaxValue = 1.0;
                context.Set<Analysis>().AddOrUpdate(Hb);

                Analysis Hb2 = new Analysis();
                Hb2.CitoID = fk.IDCito;
                Hb2.Description = "HBsAg - MEIA";
                Hb2.MinValue = 2.0;
                Hb2.MaxValue = 2.0;
                context.Set<Analysis>().AddOrUpdate(Hb2);

                Analysis Ac = new Analysis();
                Ac.CitoID = fk.IDCito;
                Ac.Description = "Ac HCV - CMIA";
                Ac.MinValue = 0.8;
                Ac.MaxValue = 1.0;
                context.Set<Analysis>().AddOrUpdate(Ac);

                Analysis Ac2 = new Analysis();
                Ac2.CitoID = fk.IDCito;
                Ac2.Description = "Ac HCV - ELISA";
                Ac2.MinValue = 1.0;
                Ac2.MaxValue = 1.0;
                context.Set<Analysis>().AddOrUpdate(Ac2);

                Analysis PRA = new Analysis();
                PRA.CitoID = fk.IDCito;
                PRA.Description = "PRA - LUMINEX";
                PRA.MinValue = 0.0;
                PRA.MaxValue = 0.0;
                context.Set<Analysis>().AddOrUpdate(PRA);
            }
            // Sample Locals
            if (!context.Set<Local>().Any())
            {
                Local a11 = new Local();
                a11.Description = "A1-1";
                context.Set<Local>().AddOrUpdate(a11);

                Local a12 = new Local();
                a12.Description = "A1-2";
                context.Set<Local>().AddOrUpdate(a12);

                Local a13 = new Local();
                a13.Description = "A1-3";
                context.Set<Local>().AddOrUpdate(a13);

                Local a14 = new Local();
                a14.Description = "A1-4";
                context.Set<Local>().AddOrUpdate(a14);

            }
        }
    }
}
    

