namespace labdsoft.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addOrgans : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrganDonateds",
                c => new
                    {
                        OrganDonatedId = c.Int(nullable: false, identity: true),
                        OrganID = c.Int(nullable: false),
                        IDPerson = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrganDonatedId)
                .ForeignKey("dbo.OrganTypes", t => t.OrganID, cascadeDelete: true)
                .ForeignKey("dbo.People", t => t.IDPerson, cascadeDelete: true)
                .Index(t => t.OrganID)
                .Index(t => t.IDPerson);
            
            CreateTable(
                "dbo.OrganNeededs",
                c => new
                    {
                        IDPerson = c.Int(nullable: false),
                        IDOrgan = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IDPerson, t.IDOrgan })
                .ForeignKey("dbo.OrganTypes", t => t.IDOrgan, cascadeDelete: true)
                .ForeignKey("dbo.People", t => t.IDPerson, cascadeDelete: true)
                .Index(t => t.IDPerson)
                .Index(t => t.IDOrgan);
            
            CreateTable(
                "dbo.WasteOrganRegs",
                c => new
                    {
                        OrganID = c.Int(nullable: false),
                        Reason = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.OrganID)
                .ForeignKey("dbo.OrganDonateds", t => t.OrganID)
                .Index(t => t.OrganID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WasteOrganRegs", "OrganID", "dbo.OrganDonateds");
            DropForeignKey("dbo.OrganNeededs", "IDPerson", "dbo.People");
            DropForeignKey("dbo.OrganNeededs", "IDOrgan", "dbo.OrganTypes");
            DropForeignKey("dbo.OrganDonateds", "IDPerson", "dbo.People");
            DropForeignKey("dbo.OrganDonateds", "OrganID", "dbo.OrganTypes");
            DropIndex("dbo.WasteOrganRegs", new[] { "OrganID" });
            DropIndex("dbo.OrganNeededs", new[] { "IDOrgan" });
            DropIndex("dbo.OrganNeededs", new[] { "IDPerson" });
            DropIndex("dbo.OrganDonateds", new[] { "IDPerson" });
            DropIndex("dbo.OrganDonateds", new[] { "OrganID" });
            DropTable("dbo.WasteOrganRegs");
            DropTable("dbo.OrganNeededs");
            DropTable("dbo.OrganDonateds");
        }
    }
}
