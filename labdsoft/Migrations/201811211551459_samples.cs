namespace labdsoft.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class samples : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SampleLocals",
                c => new
                    {
                        IDSample = c.Int(nullable: false),
                        IDLocal = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IDSample, t.IDLocal })
                .ForeignKey("dbo.Locals", t => t.IDLocal, cascadeDelete: true)
                .ForeignKey("dbo.Samples", t => t.IDSample, cascadeDelete: true)
                .Index(t => t.IDSample)
                .Index(t => t.IDLocal);
            
            CreateTable(
                "dbo.Samples",
                c => new
                    {
                        sampleId = c.Int(nullable: false, identity: true),
                        IDPerson = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.sampleId)
                .ForeignKey("dbo.People", t => t.IDPerson, cascadeDelete: true)
                .Index(t => t.IDPerson);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SampleLocals", "IDSample", "dbo.Samples");
            DropForeignKey("dbo.Samples", "IDPerson", "dbo.People");
            DropForeignKey("dbo.SampleLocals", "IDLocal", "dbo.Locals");
            DropIndex("dbo.Samples", new[] { "IDPerson" });
            DropIndex("dbo.SampleLocals", new[] { "IDLocal" });
            DropIndex("dbo.SampleLocals", new[] { "IDSample" });
            DropTable("dbo.Samples");
            DropTable("dbo.SampleLocals");
        }
    }
}
