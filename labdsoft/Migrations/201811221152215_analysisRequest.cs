namespace labdsoft.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class analysisRequest : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnalysisRequests",
                c => new
                    {
                        IDTypeAnalysis = c.Int(nullable: false),
                        IDSample = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IDTypeAnalysis, t.IDSample })
                .ForeignKey("dbo.Analyses", t => t.IDTypeAnalysis, cascadeDelete: true)
                .ForeignKey("dbo.Samples", t => t.IDSample, cascadeDelete: true)
                .Index(t => t.IDTypeAnalysis)
                .Index(t => t.IDSample);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnalysisRequests", "IDSample", "dbo.Samples");
            DropForeignKey("dbo.AnalysisRequests", "IDTypeAnalysis", "dbo.Analyses");
            DropIndex("dbo.AnalysisRequests", new[] { "IDSample" });
            DropIndex("dbo.AnalysisRequests", new[] { "IDTypeAnalysis" });
            DropTable("dbo.AnalysisRequests");
        }
    }
}
