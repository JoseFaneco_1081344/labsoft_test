namespace labdsoft.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userState : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ReceiverStates",
                c => new
                    {
                        IDPerson = c.Int(nullable: false),
                        state = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IDPerson)
                .ForeignKey("dbo.People", t => t.IDPerson)
                .Index(t => t.IDPerson);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReceiverStates", "IDPerson", "dbo.People");
            DropIndex("dbo.ReceiverStates", new[] { "IDPerson" });
            DropTable("dbo.ReceiverStates");
        }
    }
}
