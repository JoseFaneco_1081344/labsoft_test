namespace labdsoft.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tracker3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TrackerConfigs", "min_TA", c => c.Double(nullable: false));
            AlterColumn("dbo.TrackerConfigs", "max_TA", c => c.Double(nullable: false));
            AlterColumn("dbo.TrackerConfigs", "min_Temp", c => c.Double(nullable: false));
            AlterColumn("dbo.TrackerConfigs", "max_Temp", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TrackerConfigs", "max_Temp", c => c.Single(nullable: false));
            AlterColumn("dbo.TrackerConfigs", "min_Temp", c => c.Single(nullable: false));
            AlterColumn("dbo.TrackerConfigs", "max_TA", c => c.Single(nullable: false));
            AlterColumn("dbo.TrackerConfigs", "min_TA", c => c.Single(nullable: false));
        }
    }
}
