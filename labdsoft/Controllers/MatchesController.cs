﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using labdsoft.Models;

namespace labdsoft.Controllers
{
    [Authorize(Roles ="Doctor, Nurse, LabDir")]
    public class MatchesController : Controller
    {

        private static List<KeyValuePair<Receiver, double>> resultF;
        private static Match matchId;
        
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Matches
        public async Task<ActionResult> Index()
        {
            var matches = db.Matches.Include(m => m.OrganDonated);
            return View(await matches.ToListAsync());
        }

        // GET: Matches/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Match match = await db.Matches.FindAsync(id);
            if (match == null)
            {
                return HttpNotFound();
            }
            return View(match);
        }

        // GET: Matches/Create
        public ActionResult Create()
        {
            ViewBag.OrganDonatedId = new SelectList(db.OrganDonateds, "OrganDonatedId", "OrganDonatedId");
            return View();
        }

        // POST: Matches/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IdMatch,OrganDonatedId")] Match match)
        {
            /*if (ModelState.IsValid)
              {
                  db.Matches.Add(match);
                  await db.SaveChangesAsync();
                  //return RedirectToAction("Index");
                  ViewBag.OrganDonatedId = new SelectList(db.OrganDonateds, "OrganDonatedId", "OrganDonatedId");

                  return View();
              }*/

            //ViewBag.OrganDonatedId = new SelectList(db.OrganDonateds, "OrganDonatedId", "OrganDonatedId", match.OrganDonatedId);
            /*

            Dictionary<Receiver, double> result = new Dictionary<Receiver, double>();
               //FindMatch(match.OrganDonatedId);
            //Dictionary<String, String> result = new Dictionary<string, string>();

            //result.Add("Tete", "teste1");
            //result.Add("Tet2", "teste2");
            //result.Add("Tete3", "teste3");

            Receiver receiver = new Receiver();
            receiver.Name = "Claudio";
            result.Add(receiver, 12);
            */

            matchId = match;

            if (ModelState.IsValid)
            {
                db.Matches.Add(match);
                await db.SaveChangesAsync();

                Dictionary<Receiver, double> result = FindMatch(match.OrganDonatedId);

                /*
                Receiver receiver = new Receiver();
                receiver.Name = "Claudio";
                result.Add(receiver, 12);

                Receiver receiver_1 = new Receiver();
                receiver_1.Name = "Claudio";
                result.Add(receiver_1, 30);*/

                List<KeyValuePair<Receiver, double>> resultFinal = result.OrderByDescending(x => x.Value).ToList();

                resultF = resultFinal;

                ViewData["listResult"] = resultFinal;
            }                

            return View("ViewResultMatch");
        }

        // GET: Matches/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Match match = await db.Matches.FindAsync(id);
            if (match == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrganDonatedId = new SelectList(db.OrganDonateds, "OrganDonatedId", "OrganDonatedId", match.OrganDonatedId);
            return View(match);
        }

        // POST: Matches/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IdMatch,OrganDonatedId")] Match match)
        {
            if (ModelState.IsValid)
            {
                db.Entry(match).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.OrganDonatedId = new SelectList(db.OrganDonateds, "OrganDonatedId", "OrganDonatedId", match.OrganDonatedId);
            return View(match);
        }

        // GET: Matches/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Match match = await db.Matches.FindAsync(id);
            if (match == null)
            {
                return HttpNotFound();
            }
            return View(match);
        }

        // POST: Matches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Match match = await db.Matches.FindAsync(id);
            db.Matches.Remove(match);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public async Task<ActionResult> saveResultMatch()
        {
       
            foreach (KeyValuePair<labdsoft.Models.Receiver, double> item in resultF)
            {
                ReceiverMatch receiverMatch = new ReceiverMatch();

                receiverMatch.PersonId = item.Key.IDPerson;

                receiverMatch.Rank = item.Value;

                receiverMatch.MatchId = matchId.IdMatch;

                db.ReceiverMatches.Add(receiverMatch);
                await db.SaveChangesAsync();
            }
            return RedirectToAction("Index", "ReceiverMatches");

        }
        
        public Dictionary<Receiver, double> FindMatch(int organDonatedId)
        {

            List<Receiver> listFiltered = new List<Receiver>();
            OrganDonated organDonated = db.OrganDonateds.Find(organDonatedId);
            OrganType organType = organDonated.OrganType;
            //Filtrar Receivers que precisam do órgão
            List<OrganNeeded> lst = db.OrganNeededs.Where(u => u.IDOrgan == organType.OrganTypeId).ToList();
            BloodType bloodTypeDonated = db.Donors.Find(organDonated.IDPerson).BloodType;

            foreach (OrganNeeded item in lst)
            {
                ReceiverStatePo state = db.ReceiverStates.Find(item.IDPerson).state;
                //Filtrar se receiver pode ou não receber o órgão
                if ((int)state == 0 || (int)state == 1)
                {
                    Receiver receiver = db.Receivers.Find(item.IDPerson);
                    //Filtrar tipo de sangue
                    if (BloodChecker(bloodTypeDonated.Description, receiver.BloodType.Description))
                    {
                        listFiltered.Add(receiver);
                    }

                }

            }
            Donor donor = db.Donors.Find(organDonated.IDPerson);
            return MatchRank(listFiltered, donor);
        }

        private Dictionary<Receiver, double> MatchRank(List<Receiver> listFiltered, Donor donor)
        {
            Dictionary<Receiver, double> map = new Dictionary<Receiver, double>();
            HLA hlaDonor = db.HLAs.Find(donor.IDPerson);

            double rank;
            foreach (Receiver receiver in listFiltered)
            {
                rank = 0;

                HLA hlaReceiver = db.HLAs.Find(receiver.IDPerson);
                
         
                rank += HLACompatibility(hlaReceiver, hlaDonor);

                rank += PRACompatibility(receiver);

                rank += WaitTimeStartAnalysis(receiver);

                rank += GetRankCurrentDate(receiver);

                map.Add(receiver, rank);
                
            }
            return map;
        }

        public Boolean BloodChecker(string donated, string receiver)
        {
            switch (receiver)
            {
                case "O-":
                    if (donated.Equals("O-")) return true;
                    break;
                case "O+":
                    if (donated.Equals("O-") || donated.Equals("O+")) return true;
                    break;
                case "A-":
                    if (donated.Equals("O-") || donated.Equals("A-")) return true;
                    break;
                case "A+":
                    if (donated.Equals("O-") || donated.Equals("O+") || donated.Equals("A-") || donated.Equals("A+")) return true;
                    break;
                case "B-":
                    if (donated.Equals("O-") || donated.Equals("B-")) return true;
                    break;
                case "B+":
                    if (donated.Equals("O-") || donated.Equals("O+") || donated.Equals("B-") || donated.Equals("B+")) return true;
                    break;
                case "AB-":
                    if (donated.Equals("O-") || donated.Equals("A-") || donated.Equals("B-") || donated.Equals("AB-")) return true;
                    break;
                case "AB+":
                    if (donated.Equals("O-") || donated.Equals("O+")
                    || donated.Equals("B-") || donated.Equals("B+")
                    || donated.Equals("A-") || donated.Equals("A+")
                    || donated.Equals("AB-") || donated.Equals("AB+")) return true;
                    break;
                    
            }


            return true;
        }

        public int HLACompatibility(HLA hlaReceiver, HLA hlaDonor)
        {

            //A
            if (hlaReceiver.TypeA == hlaDonor.TypeA && hlaReceiver.TypeB == hlaDonor.TypeB && hlaReceiver.TypeDR == hlaDonor.TypeDR)
            {
                if (hlaReceiver.A == hlaDonor.A && hlaReceiver.B == hlaDonor.B && hlaReceiver.DR == hlaDonor.DR)
                {
                    return 12;
                }
            }

            //B
            if (hlaReceiver.TypeB == hlaDonor.TypeB && hlaReceiver.TypeDR == hlaDonor.TypeDR)
            {
                if (hlaReceiver.B == hlaDonor.B && hlaReceiver.DR == hlaDonor.DR)
                {
                    return 8;
                }
            }
            

            //C
            if (hlaReceiver.TypeB != hlaDonor.TypeB || hlaReceiver.TypeDR != hlaDonor.TypeDR)
            {
                if (hlaReceiver.B == hlaDonor.B || hlaReceiver.DR == hlaDonor.DR)
                {
                    return 4;
                }
            }

            if (hlaReceiver.TypeB == hlaDonor.TypeB || hlaReceiver.TypeDR == hlaDonor.TypeDR)
            {
                if (hlaReceiver.B != hlaDonor.B || hlaReceiver.DR != hlaDonor.DR)
                {
                    return 4;
                }
            }


            //D
            if (hlaReceiver.TypeB != hlaDonor.TypeB && hlaReceiver.TypeDR != hlaDonor.TypeDR)
            {
                if (hlaReceiver.B == hlaDonor.B || hlaReceiver.DR == hlaDonor.DR)
                {
                    return 2;
                }
            }

            if (hlaReceiver.TypeB == hlaDonor.TypeB || hlaReceiver.TypeDR == hlaDonor.TypeDR)
            {
                if (hlaReceiver.B != hlaDonor.B && hlaReceiver.DR != hlaDonor.DR)
                {
                    return 2;
                }
            }

            if (hlaReceiver.TypeB != hlaDonor.TypeB || hlaReceiver.TypeDR == hlaDonor.TypeDR)
            {
                if (hlaReceiver.B == hlaDonor.B && hlaReceiver.DR != hlaDonor.DR)
                {
                    return 2;
                }
            }

            if (hlaReceiver.TypeB == hlaDonor.TypeB || hlaReceiver.TypeDR != hlaDonor.TypeDR)
            {
                if (hlaReceiver.B != hlaDonor.B && hlaReceiver.DR == hlaDonor.DR)
                {
                    return 2;
                }
            }

            //E

            if (hlaReceiver.TypeB != hlaDonor.TypeB && hlaReceiver.TypeDR != hlaDonor.TypeDR)
            {
                if (hlaReceiver.B != hlaDonor.B && hlaReceiver.DR != hlaDonor.DR)
                {
                    return 1;
                }
            }
            
            return 0;
        }

        private int PRACompatibility(Receiver receiver)
        {
            Analysis a = db.Analyses.Where(u => u.Description.Equals("PRA - LUMINEX")).First();
            List<AnalysisPPerson> lstReceiverAnalysis = db.AnalysisPPersons.Where(z => z.IDPerson == receiver.IDPerson).ToList();
            
            if (lstReceiverAnalysis!=null && lstReceiverAnalysis.Count!=0)
            {
                double result = lstReceiverAnalysis.Where(i => i.TypeAnalysis == a.IDAnalysis).ToList().First().Result;

                if (result >= 80)
                {
                    return 8;
                }
                else if (result >= 80)
                {
                    return 5;
                }
            }
            return 0;
        }

        public double WaitTimeStartAnalysis(Receiver receiver)
        {

            DateTime today = DateTime.Today;

            double totalDays = (today - receiver.InDate).TotalDays;

            double totalMonths = totalDays / 30;
            
            return (totalMonths * 0.1);  
        }

        public int GetRankCurrentDate(Receiver receiver)
        { 
            DateTime today = DateTime.Today;
            
            var a = (today.Year * 100 + today.Month) * 100 + today.Day;
            var b = (receiver.BornDate.Year * 100 + receiver.BornDate.Month) * 100 + receiver.BornDate.Day;

            int age =  (a - b) / 10000;

            if (age < 11)
            {
                return 5;
            } else if (age >=11 || age <= 18)
            {
                return 4;
            }

            return 0;
        }
    }
}
