﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using labdsoft.Models;

namespace labdsoft.Controllers
{
    [Authorize(Roles ="LabDir")]
    public class AnalysesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Analyses
        public async Task<ActionResult> Index()
        {
            var analyses = db.Analyses.Include(a => a.IDCito);
            return View(await analyses.ToListAsync());
        }

        // GET: Analyses/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Analysis analysis = await db.Analyses.FindAsync(id);
            if (analysis == null)
            {
                return HttpNotFound();
            }
            return View(analysis);
        }

        // GET: Analyses/Create
        public ActionResult Create()
        {
            ViewBag.CitoID = new SelectList(db.CITOes, "IDCito", "Local");
            return View();
        }

        // POST: Analyses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDAnalysis,CitoID,Description,MinValue,MaxValue")] Analysis analysis)
        {
            if (ModelState.IsValid)
            {
                db.Analyses.Add(analysis);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.CitoID = new SelectList(db.CITOes, "IDCito", "Local", analysis.CitoID);
            return View(analysis);
        }

        // GET: Analyses/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Analysis analysis = await db.Analyses.FindAsync(id);
            if (analysis == null)
            {
                return HttpNotFound();
            }
            ViewBag.CitoID = new SelectList(db.CITOes, "IDCito", "Local", analysis.CitoID);
            return View(analysis);
        }

        // POST: Analyses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDAnalysis,CitoID,Description,MinValue,MaxValue")] Analysis analysis)
        {
            if (ModelState.IsValid)
            {
                db.Entry(analysis).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CitoID = new SelectList(db.CITOes, "IDCito", "Local", analysis.CitoID);
            return View(analysis);
        }

        // GET: Analyses/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Analysis analysis = await db.Analyses.FindAsync(id);
            if (analysis == null)
            {
                return HttpNotFound();
            }
            return View(analysis);
        }

        // POST: Analyses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Analysis analysis = await db.Analyses.FindAsync(id);
            db.Analyses.Remove(analysis);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
