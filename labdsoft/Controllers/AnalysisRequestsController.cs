﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using labdsoft.Models;
using System.Net.Http;

namespace labdsoft.Controllers
{
    [Authorize(Roles = "Doctor, Nurse, LabTec, LabDir")]
    public class AnalysisRequestsController : Controller
    {
        private HttpClient client = new HttpClient();
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: AnalysisRequests
        public async Task<ActionResult> Index()
        {
            var analysisRequests = db.AnalysisRequests.Include(a => a.IDAnalysis).Include(a => a.Sample);
            return View(await analysisRequests.ToListAsync());
        }

        // GET: AnalysisRequests/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnalysisRequest analysisRequest = await db.AnalysisRequests.FindAsync(id);
            if (analysisRequest == null)
            {
                return HttpNotFound();
            }
            return View(analysisRequest);
        }

        // GET: AnalysisRequests/Create
        public ActionResult Create()
        {
            ViewBag.IDTypeAnalysis = new SelectList(db.Analyses, "IDAnalysis", "Description");
            ViewBag.IDSample = new SelectList(db.Samples, "sampleId", "sampleId");
            return View();
        }

        // POST: AnalysisRequests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDTypeAnalysis,IDSample")] AnalysisRequest analysisRequest)
        {
            if (ModelState.IsValid)
            {
                var values = new Dictionary<string, string>{
                   { "SampleID", "\"" + analysisRequest.IDSample +"\"" },
                   { "AnalysisID", "\"" + analysisRequest.IDTypeAnalysis +"\"" }
                };
                var content = new FormUrlEncodedContent(values);
                HttpResponseMessage response = await client.PostAsync(
                "http://localhost:55831/api/AnalysisRequests/Create", content);
                response.EnsureSuccessStatusCode();
                db.AnalysisRequests.Add(analysisRequest);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.IDTypeAnalysis = new SelectList(db.Analyses, "IDAnalysis", "Description", analysisRequest.IDTypeAnalysis);
            ViewBag.IDSample = new SelectList(db.Samples, "sampleId", "sampleId", analysisRequest.IDSample);
            return View(analysisRequest);
        }

        // GET: AnalysisRequests/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnalysisRequest analysisRequest = await db.AnalysisRequests.FindAsync(id);
            if (analysisRequest == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDTypeAnalysis = new SelectList(db.Analyses, "IDAnalysis", "Description", analysisRequest.IDTypeAnalysis);
            ViewBag.IDSample = new SelectList(db.Samples, "sampleId", "sampleId", analysisRequest.IDSample);
            return View(analysisRequest);
        }

        // POST: AnalysisRequests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDTypeAnalysis,IDSample")] AnalysisRequest analysisRequest)
        {
            if (ModelState.IsValid)
            {
                db.Entry(analysisRequest).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IDTypeAnalysis = new SelectList(db.Analyses, "IDAnalysis", "Description", analysisRequest.IDTypeAnalysis);
            ViewBag.IDSample = new SelectList(db.Samples, "sampleId", "sampleId", analysisRequest.IDSample);
            return View(analysisRequest);
        }

        // GET: AnalysisRequests/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnalysisRequest analysisRequest = await db.AnalysisRequests.FindAsync(id);
            if (analysisRequest == null)
            {
                return HttpNotFound();
            }
            return View(analysisRequest);
        }

        // POST: AnalysisRequests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AnalysisRequest analysisRequest = await db.AnalysisRequests.FindAsync(id);
            db.AnalysisRequests.Remove(analysisRequest);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
