﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using labdsoft.Models;

namespace labdsoft.Controllers
{
    [Authorize(Roles ="PickTec, Doctor, Nurse")]
    public class DonorsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Donors
        public async Task<ActionResult> Index()
        {
            var donors = db.Donors.Include(d => d.BloodType);
            return View(await donors.ToListAsync());
        }

        // GET: Donors/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Donor donor = await db.Donors.FindAsync(id);
            if (donor == null)
            {
                return HttpNotFound();
            }
            return View(donor);
        }

        // GET: Donors/Create
        public ActionResult Create()
        {
            ViewBag.BloodtypeID = new SelectList(db.BloodTypes, "BloodTypeID", "Description");
            return View();
        }

        // POST: Donors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDPerson,DeadDate,CollectionDate,Category,Observations,WorkerID,Name,BloodtypeID")] Donor donor)
        {
            if (ModelState.IsValid)
            {
                db.Donors.Add(donor);
                await db.SaveChangesAsync();
                return RedirectToAction("Create", "OrganDonateds");
            }

            ViewBag.BloodtypeID = new SelectList(db.BloodTypes, "BloodTypeID", "Description", donor.BloodtypeID);
            return View(donor);
        }

        // GET: Donors/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Donor donor = await db.Donors.FindAsync(id);
            if (donor == null)
            {
                return HttpNotFound();
            }
            ViewBag.BloodtypeID = new SelectList(db.BloodTypes, "BloodTypeID", "Description", donor.BloodtypeID);
            return View(donor);
        }

        // POST: Donors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDPerson,DeadDate,CollectionDate,Category,Observations,WorkerID,Name,BloodtypeID")] Donor donor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(donor).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.BloodtypeID = new SelectList(db.BloodTypes, "BloodTypeID", "Description", donor.BloodtypeID);
            return View(donor);
        }

        // GET: Donors/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Donor donor = await db.Donors.FindAsync(id);
            if (donor == null)
            {
                return HttpNotFound();
            }
            return View(donor);
        }

        // POST: Donors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Donor donor = await db.Donors.FindAsync(id);
            db.Donors.Remove(donor);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
