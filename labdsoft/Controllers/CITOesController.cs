﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using labdsoft.Models;

namespace labdsoft.Controllers
{
    public class CITOesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: CITOes
        public async Task<ActionResult> Index()
        {
            return View(await db.CITOes.ToListAsync());
        }

        // GET: CITOes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CITO cITO = await db.CITOes.FindAsync(id);
            if (cITO == null)
            {
                return HttpNotFound();
            }
            return View(cITO);
        }

        // GET: CITOes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CITOes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDCito,Local,OrganLifeTime")] CITO cITO)
        {
            if (ModelState.IsValid)
            {
                db.CITOes.Add(cITO);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(cITO);
        }

        // GET: CITOes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CITO cITO = await db.CITOes.FindAsync(id);
            if (cITO == null)
            {
                return HttpNotFound();
            }
            return View(cITO);
        }

        // POST: CITOes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDCito,Local,OrganLifeTime")] CITO cITO)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cITO).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(cITO);
        }

        // GET: CITOes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CITO cITO = await db.CITOes.FindAsync(id);
            if (cITO == null)
            {
                return HttpNotFound();
            }
            return View(cITO);
        }

        // POST: CITOes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            CITO cITO = await db.CITOes.FindAsync(id);
            db.CITOes.Remove(cITO);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
