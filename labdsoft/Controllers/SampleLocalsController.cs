﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using labdsoft.Models;

namespace labdsoft.Controllers
{
    [Authorize(Roles ="LabTec, LabDir")]
    public class SampleLocalsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: SampleLocals
        public async Task<ActionResult> Index()
        {
            var sampleLocals = db.SampleLocals.Include(s => s.LocalId).Include(s => s.SampleId);           
            return View(await sampleLocals.ToListAsync());
        }

        // GET: SampleLocals/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SampleLocal sampleLocal = await db.SampleLocals.FindAsync(id);
            if (sampleLocal == null)
            {
                return HttpNotFound();
            }
            return View(sampleLocal);
        }

        // GET: SampleLocals/Create
        public ActionResult Create()
        {
            List<Local> locals = db.Locals.ToList();
            List<SampleLocal> sampleLocal = db.SampleLocals.ToList();
            bool occupied;
            foreach (SampleLocal sample in sampleLocal)
            {
                Local l = locals.Find(i => i.LocalID == sample.IDLocal);
                locals.Remove(l);
            }

            
            ViewBag.lst = locals;
            ViewBag.IDLocal = new SelectList(locals, "LocalID", "Description");
            ViewBag.IDSample = new SelectList(db.Samples, "sampleId", "sampleId");
            return View();
        }

        // POST: SampleLocals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDSample,IDLocal")] SampleLocal sampleLocal)
        {
            if (ModelState.IsValid)
            {
                db.SampleLocals.Add(sampleLocal);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.IDLocal = new SelectList(db.Locals, "LocalID", "Description", sampleLocal.IDLocal);
            ViewBag.IDSample = new SelectList(db.Samples, "sampleId", "sampleId", sampleLocal.IDSample);
            return View(sampleLocal);
        }

        // GET: SampleLocals/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SampleLocal sampleLocal = await db.SampleLocals.FindAsync(id);
            if (sampleLocal == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDLocal = new SelectList(db.Locals, "LocalID", "Description", sampleLocal.IDLocal);
            ViewBag.IDSample = new SelectList(db.Samples, "sampleId", "sampleId", sampleLocal.IDSample);
            return View(sampleLocal);
        }

        // POST: SampleLocals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDSample,IDLocal")] SampleLocal sampleLocal)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sampleLocal).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IDLocal = new SelectList(db.Locals, "LocalID", "Description", sampleLocal.IDLocal);
            ViewBag.IDSample = new SelectList(db.Samples, "sampleId", "sampleId", sampleLocal.IDSample);
            return View(sampleLocal);
        }

        // GET: SampleLocals/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SampleLocal sampleLocal = await db.SampleLocals.FindAsync(id);
            if (sampleLocal == null)
            {
                return HttpNotFound();
            }
            return View(sampleLocal);
        }

        // POST: SampleLocals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            SampleLocal sampleLocal = await db.SampleLocals.FindAsync(id);
            db.SampleLocals.Remove(sampleLocal);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
