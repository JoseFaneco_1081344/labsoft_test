﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using labdsoft.Models;

namespace labdsoft.Controllers
{
    public class BloodTypesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: BloodTypes
        public async Task<ActionResult> Index()
        {
            return View(await db.BloodTypes.ToListAsync());
        }

        // GET: BloodTypes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BloodType bloodType = await db.BloodTypes.FindAsync(id);
            if (bloodType == null)
            {
                return HttpNotFound();
            }
            return View(bloodType);
        }

        // GET: BloodTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BloodTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "BloodTypeID,Description")] BloodType bloodType)
        {
            if (ModelState.IsValid)
            {
                db.BloodTypes.Add(bloodType);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(bloodType);
        }

        // GET: BloodTypes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BloodType bloodType = await db.BloodTypes.FindAsync(id);
            if (bloodType == null)
            {
                return HttpNotFound();
            }
            return View(bloodType);
        }

        // POST: BloodTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "BloodTypeID,Description")] BloodType bloodType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bloodType).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(bloodType);
        }

        // GET: BloodTypes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BloodType bloodType = await db.BloodTypes.FindAsync(id);
            if (bloodType == null)
            {
                return HttpNotFound();
            }
            return View(bloodType);
        }

        // POST: BloodTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            BloodType bloodType = await db.BloodTypes.FindAsync(id);
            db.BloodTypes.Remove(bloodType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
