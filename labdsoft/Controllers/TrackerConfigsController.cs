﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using labdsoft.Models;

namespace labdsoft.Controllers
{
    [Authorize (Roles = "Doctor")]
    public class TrackerConfigsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TrackerConfigs
        public async Task<ActionResult> Index()
        {
            var trackerConfigs = db.TrackerConfigs.Include(t => t.TrackerID);
            return View(await trackerConfigs.ToListAsync());
        }

        // GET: TrackerConfigs/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrackerConfig trackerConfig = await db.TrackerConfigs.FindAsync(id);
            if (trackerConfig == null)
            {
                return HttpNotFound();
            }
            return View(trackerConfig);
        }

        // GET: TrackerConfigs/Create
        public ActionResult Create()
        {
            ViewBag.IDTracker = new SelectList(db.ReceiverTrackers, "TrackerID", "TrackerID");
            return View();
        }

        // POST: TrackerConfigs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDTracker,min_TA,max_TA,min_Temp,max_Temp,min_HR,max_HR")] TrackerConfig trackerConfig)
        {
            if (ModelState.IsValid)
            {
                TrackerConfig tcfg = await db.TrackerConfigs.FindAsync(trackerConfig.IDTracker);
                if (tcfg != null)
                {
                    db.TrackerConfigs.Remove(tcfg);
                }
                
                db.TrackerConfigs.Add(trackerConfig);
                await db.SaveChangesAsync();
                
                return RedirectToAction("Index");
            }

            ViewBag.IDTracker = new SelectList(db.ReceiverTrackers, "TrackerID", "TrackerID", trackerConfig.IDTracker);
            return View(trackerConfig);
        }

        // GET: TrackerConfigs/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrackerConfig trackerConfig = await db.TrackerConfigs.FindAsync(id);
            if (trackerConfig == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDTracker = new SelectList(db.ReceiverTrackers, "TrackerID", "TrackerID", trackerConfig.IDTracker);
            return View(trackerConfig);
        }

        // POST: TrackerConfigs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDTracker,min_TA,max_TA,min_Temp,max_Temp,min_HR,max_HR")] TrackerConfig trackerConfig)
        {
            if (ModelState.IsValid)
            {
                db.Entry(trackerConfig).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IDTracker = new SelectList(db.ReceiverTrackers, "TrackerID", "TrackerID", trackerConfig.IDTracker);
            return View(trackerConfig);
        }

        // GET: TrackerConfigs/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrackerConfig trackerConfig = await db.TrackerConfigs.FindAsync(id);
            if (trackerConfig == null)
            {
                return HttpNotFound();
            }
            return View(trackerConfig);
        }

        // POST: TrackerConfigs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            TrackerConfig trackerConfig = await db.TrackerConfigs.FindAsync(id);
            db.TrackerConfigs.Remove(trackerConfig);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
