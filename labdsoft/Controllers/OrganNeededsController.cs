﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using labdsoft.Models;

namespace labdsoft.Controllers
{
    public class OrganNeededsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: OrganNeededs
        public async Task<ActionResult> Index()
        {
            var organNeededs = db.OrganNeededs.Include(o => o.OrganType).Include(o => o.Person);
            return View(await organNeededs.ToListAsync());
        }

        // GET: OrganNeededs/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganNeeded organNeeded = await db.OrganNeededs.FindAsync(id);
            if (organNeeded == null)
            {
                return HttpNotFound();
            }
            return View(organNeeded);
        }

        // GET: OrganNeededs/Create
        public ActionResult Create()
        {
            ViewBag.IDOrgan = new SelectList(db.OrganTypes, "OrganTypeId", "Description");
            ViewBag.IDPerson = new SelectList(db.Receivers, "IDPerson", "Name");
            return View();
        }

        // POST: OrganNeededs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDPerson,IDOrgan")] OrganNeeded organNeeded)
        {
            if (ModelState.IsValid)
            {
                db.OrganNeededs.Add(organNeeded);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.IDOrgan = new SelectList(db.OrganTypes, "OrganTypeId", "Description", organNeeded.IDOrgan);
            ViewBag.IDPerson = new SelectList(db.Receivers, "IDPerson", "Name", organNeeded.IDPerson);
            return View(organNeeded);
        }

        // GET: OrganNeededs/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganNeeded organNeeded = await db.OrganNeededs.FindAsync(id);
            if (organNeeded == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDOrgan = new SelectList(db.OrganTypes, "OrganTypeId", "Description", organNeeded.IDOrgan);
            ViewBag.IDPerson = new SelectList(db.Receivers, "IDPerson", "Name", organNeeded.IDPerson);
            return View(organNeeded);
        }

        // POST: OrganNeededs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDPerson,IDOrgan")] OrganNeeded organNeeded)
        {
            if (ModelState.IsValid)
            {
                db.Entry(organNeeded).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IDOrgan = new SelectList(db.OrganTypes, "OrganTypeId", "Description", organNeeded.IDOrgan);
            ViewBag.IDPerson = new SelectList(db.Receivers, "IDPerson", "NAme", organNeeded.IDPerson);
            return View(organNeeded);
        }

        // GET: OrganNeededs/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganNeeded organNeeded = await db.OrganNeededs.FindAsync(id);
            if (organNeeded == null)
            {
                return HttpNotFound();
            }
            return View(organNeeded);
        }

        // POST: OrganNeededs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            OrganNeeded organNeeded = await db.OrganNeededs.FindAsync(id);
            db.OrganNeededs.Remove(organNeeded);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
