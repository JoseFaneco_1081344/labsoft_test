﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using labdsoft.Models;

namespace labdsoft.Controllers
{
    [Authorize(Roles ="LabTec, LabDir")]
    public class AnalysisPPersonsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: AnalysisPPersons
        public async Task<ActionResult> Index()
        {
            var analysisPPersons = db.AnalysisPPersons.Include(a => a.IDAnalysis).Include(a => a.Person);
            return View(await analysisPPersons.ToListAsync());
        }

        // GET: AnalysisPPersons/Details/5
        public async Task<ActionResult> Details(DateTime id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnalysisPPerson analysisPPerson = await db.AnalysisPPersons.FindAsync(id);
            if (analysisPPerson == null)
            {
                return HttpNotFound();
            }
            return View(analysisPPerson);
        }

        // GET: AnalysisPPersons/Create
        public ActionResult Create()
        {
            ViewBag.TypeAnalysis = new SelectList(db.Analyses, "IDAnalysis", "Description");
            ViewBag.IDPerson = new SelectList(db.People, "IDPerson", "Name");
            return View();
        }

        // POST: AnalysisPPersons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Date,IDPerson,TypeAnalysis,Result,state")] AnalysisPPerson analysisPPerson)
        {
            if (ModelState.IsValid )
            {
                db.AnalysisPPersons.Add(analysisPPerson);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.TypeAnalysis = new SelectList(db.Analyses, "IDAnalysis", "Description", analysisPPerson.TypeAnalysis);
            ViewBag.IDPerson = new SelectList(db.People, "IDPerson", "Name", analysisPPerson.IDPerson);
            
            return View(analysisPPerson);
        }       



        // GET: AnalysisPPersons/Edit/5
        public async Task<ActionResult> Edit(int id, DateTime date)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<AnalysisPPerson> LstanalysisPPerson = db.AnalysisPPersons.ToList();
            AnalysisPPerson analysisPPerson = null;
            foreach (AnalysisPPerson analysis in LstanalysisPPerson)
            {
                if(analysis.Date == date && analysis.IDPerson == id)
                {
                    analysisPPerson = analysis;
                }
            }

            if (analysisPPerson == null)
            {
                return HttpNotFound();
            }
            ViewBag.TypeAnalysis = new SelectList(db.Analyses, "IDAnalysis", "Description", analysisPPerson.TypeAnalysis);
            ViewBag.IDPerson = new SelectList(db.People, "IDPerson", "Name", analysisPPerson.IDPerson);
            return View(analysisPPerson);
        }

        // POST: AnalysisPPersons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Date,IDPerson,TypeAnalysis,Result,state")] AnalysisPPerson analysisPPerson)
        {
            DateTime a = analysisPPerson.Date;
            String.Format("{0:dd/MM/yyyy}", a);
            if (ModelState.IsValid)
            {
                List<AnalysisPPerson> lst =  db.AnalysisPPersons.ToList();
                foreach (AnalysisPPerson analysis in lst)
                {   
                    if(analysis.Date == a && analysis.IDPerson == analysisPPerson.IDPerson)
                    {
                        db.AnalysisPPersons.Remove(analysis);
                    }
                }
                db.AnalysisPPersons.Add(analysisPPerson);           
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.TypeAnalysis = new SelectList(db.Analyses, "IDAnalysis", "Description", analysisPPerson.TypeAnalysis);
            ViewBag.IDPerson = new SelectList(db.People, "IDPerson", "Name", analysisPPerson.IDPerson);
            return View(analysisPPerson);
        }

        // GET: AnalysisPPersons/Delete/5
        public async Task<ActionResult> Delete(DateTime id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnalysisPPerson analysisPPerson = await db.AnalysisPPersons.FindAsync(id);
            if (analysisPPerson == null)
            {
                return HttpNotFound();
            }
            return View(analysisPPerson);
        }

        // POST: AnalysisPPersons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(DateTime id)
        {
            AnalysisPPerson analysisPPerson = await db.AnalysisPPersons.FindAsync(id);
            db.AnalysisPPersons.Remove(analysisPPerson);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
