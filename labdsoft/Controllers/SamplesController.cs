﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using labdsoft.Models;

namespace labdsoft.Controllers
{
    [Authorize(Roles ="Doctor, Nurse, LabDir, LabTec")]
    public class SamplesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Samples
        public async Task<ActionResult> Index()
        {
            var samples = db.Samples.Include(s => s.Person);
            return View(await samples.ToListAsync());
        }

        // GET: Samples/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sample sample = await db.Samples.FindAsync(id);
            if (sample == null)
            {
                return HttpNotFound();
            }
            return View(sample);
        }

        // GET: Samples/Create
        public ActionResult Create()
        {
            ViewBag.IDPerson = new SelectList(db.People, "IDPerson", "Name");
            return View();
        }

        // POST: Samples/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "sampleId,IDPerson")] Sample sample)
        {
            if (ModelState.IsValid)
            {
                db.Samples.Add(sample);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.IDPerson = new SelectList(db.People, "IDPerson", "Name", sample.IDPerson);
            return View(sample);
        }

        // GET: Samples/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sample sample = await db.Samples.FindAsync(id);
            if (sample == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDPerson = new SelectList(db.People, "IDPerson", "Name", sample.IDPerson);
            return View(sample);
        }

        // POST: Samples/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "sampleId,IDPerson")] Sample sample)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sample).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IDPerson = new SelectList(db.People, "IDPerson", "Name", sample.IDPerson);
            return View(sample);
        }

        // GET: Samples/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sample sample = await db.Samples.FindAsync(id);
            if (sample == null)
            {
                return HttpNotFound();
            }
            return View(sample);
        }

        // POST: Samples/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Sample sample = await db.Samples.FindAsync(id);
            db.Samples.Remove(sample);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
