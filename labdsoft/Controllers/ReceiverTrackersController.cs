﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using labdsoft.Models;

namespace labdsoft.Controllers
{
    [Authorize(Roles = "Doctor, Nurse")]
    public class ReceiverTrackersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ReceiverTrackers
        public async Task<ActionResult> Index()
        {
            var receiverTrackers = db.ReceiverTrackers.Include(r => r.Person);
            return View(await receiverTrackers.ToListAsync());
        }

        // GET: ReceiverTrackers/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReceiverTracker receiverTracker = await db.ReceiverTrackers.FindAsync(id);
            if (receiverTracker == null)
            {
                return HttpNotFound();
            }
            return View(receiverTracker);
        }

        // GET: ReceiverTrackers/Create
        public ActionResult Create()
        {
            ViewBag.IDPerson = new SelectList(db.People, "IDPerson", "Name");
            return View();
        }

        // POST: ReceiverTrackers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "TrackerID,IDPerson")] ReceiverTracker receiverTracker)
        {
            if (ModelState.IsValid)
            {
                db.ReceiverTrackers.Add(receiverTracker);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.IDPerson = new SelectList(db.People, "IDPerson", "Name", receiverTracker.IDPerson);
            return View(receiverTracker);
        }

        // GET: ReceiverTrackers/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReceiverTracker receiverTracker = await db.ReceiverTrackers.FindAsync(id);
            if (receiverTracker == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDPerson = new SelectList(db.People, "IDPerson", "Name", receiverTracker.IDPerson);
            return View(receiverTracker);
        }

        // POST: ReceiverTrackers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "TrackerID,IDPerson")] ReceiverTracker receiverTracker)
        {
            if (ModelState.IsValid)
            {
                db.Entry(receiverTracker).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IDPerson = new SelectList(db.People, "IDPerson", "Name", receiverTracker.IDPerson);
            return View(receiverTracker);
        }

        // GET: ReceiverTrackers/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReceiverTracker receiverTracker = await db.ReceiverTrackers.FindAsync(id);
            if (receiverTracker == null)
            {
                return HttpNotFound();
            }
            return View(receiverTracker);
        }

        // POST: ReceiverTrackers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            ReceiverTracker receiverTracker = await db.ReceiverTrackers.FindAsync(id);
            db.ReceiverTrackers.Remove(receiverTracker);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
