﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace labdsoft.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            string user = User.Identity.Name;
            if (user.Equals("doctor@cito.pt"))
            {
                return View("IndexDoctor");
            }else if (user.Equals("nurse@cito.pt"))
            {
                return View("IndexNurse");
            }
            else if (user.Equals("labtec@cito.pt"))
            {
                return View("IndexLabTec");
            }else if (user.Equals("labdir@cito.pt"))
            {
                return View("IndexLabDir");
            }else if (user.Equals("picktec@cito.pt"))
            {
                return View("IndexPickTec");
            }

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}