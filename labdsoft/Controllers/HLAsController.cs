﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using labdsoft.Models;

namespace labdsoft.Controllers
{
    public class HLAsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: HLAs
        public async Task<ActionResult> Index()
        {
            var hLAs = db.HLAs.Include(h => h.Person);
            return View(await hLAs.ToListAsync());
        }

        // GET: HLAs/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HLA hLA = await db.HLAs.FindAsync(id);
            if (hLA == null)
            {
                return HttpNotFound();
            }
            return View(hLA);
        }

        // GET: HLAs/Create
        public ActionResult Create()
        {
            ViewBag.IDPerson = new SelectList(db.People, "IDPerson", "Name");
            return View();
        }

        // POST: HLAs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDPerson,TypeA,TypeB,TypeC,TypeDR,TypeDQ,A,B,C,DR,DQ")] HLA hLA)
        {
            if (ModelState.IsValid)
            {
                db.HLAs.Add(hLA);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.IDPerson = new SelectList(db.People, "IDPerson", "Name", hLA.IDPerson);
            return View(hLA);
        }

        // GET: HLAs/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HLA hLA = await db.HLAs.FindAsync(id);
            if (hLA == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDPerson = new SelectList(db.People, "IDPerson", "Name", hLA.IDPerson);
            return View(hLA);
        }

        // POST: HLAs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDPerson,TypeA,TypeB,TypeC,TypeDR,TypeDQ,A,B,C,DR,DQ")] HLA hLA)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hLA).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IDPerson = new SelectList(db.People, "IDPerson", "Name", hLA.IDPerson);
            return View(hLA);
        }

        // GET: HLAs/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HLA hLA = await db.HLAs.FindAsync(id);
            if (hLA == null)
            {
                return HttpNotFound();
            }
            return View(hLA);
        }

        // POST: HLAs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            HLA hLA = await db.HLAs.FindAsync(id);
            db.HLAs.Remove(hLA);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
