﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using labdsoft.Models;

namespace labdsoft.Controllers
{
    [Authorize(Roles = "Doctor")]
    public class ReceiverStatesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ReceiverStates
        public async Task<ActionResult> Index()
        {
            var receiverStates = db.ReceiverStates.Include(r => r.Person);
            return View(await receiverStates.ToListAsync());
        }

        // GET: ReceiverStates/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReceiverState receiverState = await db.ReceiverStates.FindAsync(id);
            if (receiverState == null)
            {
                return HttpNotFound();
            }
            return View(receiverState);
        }

        // GET: ReceiverStates/Create
        public ActionResult Create()
        {
            ViewBag.IDPerson = new SelectList(db.Receivers, "IDPerson", "Name");
            return View();
        }

        // POST: ReceiverStates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDPerson,state")] ReceiverState receiverState)
        {
            if (ModelState.IsValid)
            {
                if (db.ReceiverStates.Find(receiverState.IDPerson) == null)
                {
                    db.ReceiverStates.Add(receiverState);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                else
                {
                    db.ReceiverStates.Remove(db.ReceiverStates.Find(receiverState.IDPerson));
                    db.ReceiverStates.Add(receiverState);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                
            }

            ViewBag.IDPerson = new SelectList(db.Receivers, "IDPerson", "Name", receiverState.IDPerson);
            return View(receiverState);
        }

        // GET: ReceiverStates/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReceiverState receiverState = await db.ReceiverStates.FindAsync(id);
            if (receiverState == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDPerson = new SelectList(db.Receivers, "IDPerson", "Name", receiverState.IDPerson);
            return View(receiverState);
        }

        // POST: ReceiverStates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDPerson,state")] ReceiverState receiverState)
        {
            if (ModelState.IsValid)
            {
                db.Entry(receiverState).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IDPerson = new SelectList(db.Receivers, "IDPerson", "Name", receiverState.IDPerson);
            return View(receiverState);
        }

        // GET: ReceiverStates/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReceiverState receiverState = await db.ReceiverStates.FindAsync(id);
            if (receiverState == null)
            {
                return HttpNotFound();
            }
            return View(receiverState);
        }

        // POST: ReceiverStates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ReceiverState receiverState = await db.ReceiverStates.FindAsync(id);
            db.ReceiverStates.Remove(receiverState);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
