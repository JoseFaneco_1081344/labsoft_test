﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using labdsoft.Models;

namespace labdsoft.Controllers
{
    [Authorize(Roles = "Doctor, Nurse, LabDir")]
    public class ReceiverMatchesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ReceiverMatches
        public async Task<ActionResult> Index()
        {
            var receiverMatches = db.ReceiverMatches.Include(r => r.Match).Include(r => r.Person);
            return View(await receiverMatches.ToListAsync());
        }

        // GET: ReceiverMatches/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReceiverMatch receiverMatch = await db.ReceiverMatches.FindAsync(id);
            if (receiverMatch == null)
            {
                return HttpNotFound();
            }
            return View(receiverMatch);
        }

        // GET: ReceiverMatches/Create
        public ActionResult Create()
        {
            ViewBag.MatchId = new SelectList(db.Matches, "IdMatch", "IdMatch");
            ViewBag.PersonId = new SelectList(db.People, "IDPerson", "WorkerID");
            return View();
        }

        // POST: ReceiverMatches/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IdReceiverMatch,PersonId,MatchId,Rank")] ReceiverMatch receiverMatch)
        {
            if (ModelState.IsValid)
            {
                db.ReceiverMatches.Add(receiverMatch);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.MatchId = new SelectList(db.Matches, "IdMatch", "IdMatch", receiverMatch.MatchId);
            ViewBag.PersonId = new SelectList(db.People, "IDPerson", "WorkerID", receiverMatch.PersonId);
            return View(receiverMatch);
        }

        // GET: ReceiverMatches/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReceiverMatch receiverMatch = await db.ReceiverMatches.FindAsync(id);
            if (receiverMatch == null)
            {
                return HttpNotFound();
            }
            ViewBag.MatchId = new SelectList(db.Matches, "IdMatch", "IdMatch", receiverMatch.MatchId);
            ViewBag.PersonId = new SelectList(db.People, "IDPerson", "WorkerID", receiverMatch.PersonId);
            return View(receiverMatch);
        }

        // POST: ReceiverMatches/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IdReceiverMatch,PersonId,MatchId,Rank")] ReceiverMatch receiverMatch)
        {
            if (ModelState.IsValid)
            {
                db.Entry(receiverMatch).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.MatchId = new SelectList(db.Matches, "IdMatch", "IdMatch", receiverMatch.MatchId);
            ViewBag.PersonId = new SelectList(db.People, "IDPerson", "WorkerID", receiverMatch.PersonId);
            return View(receiverMatch);
        }

        // GET: ReceiverMatches/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReceiverMatch receiverMatch = await db.ReceiverMatches.FindAsync(id);
            if (receiverMatch == null)
            {
                return HttpNotFound();
            }
            return View(receiverMatch);
        }

        // POST: ReceiverMatches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ReceiverMatch receiverMatch = await db.ReceiverMatches.FindAsync(id);
            db.ReceiverMatches.Remove(receiverMatch);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
