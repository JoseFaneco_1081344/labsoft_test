﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using labdsoft.Models;

namespace labdsoft.Controllers
{
    [Authorize(Roles ="Doctor, Nurse")]
    public class ReceiversController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Receivers
        public async Task<ActionResult> Index()
        {
            var receivers = db.Receivers.Include(r => r.BloodType);
            return View(await receivers.ToListAsync());
        }

        // GET: Receivers/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Receiver receiver = await db.Receivers.FindAsync(id);
            if (receiver == null)
            {
                return HttpNotFound();
            }
            return View(receiver);
        }

        // GET: Receivers/Create
        public ActionResult Create()
        {
            ViewBag.BloodtypeID = new SelectList(db.BloodTypes, "BloodTypeID", "Description");
            return View();
        }

        // POST: Receivers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDPerson,InDate,adress,BornDate,cc,Nationality,IMC,WorkerID,Name,BloodtypeID")] Receiver receiver)
        {
            receiver.InDate = DateTime.Today;
            
            if (ModelState.IsValid)
            {
                db.Receivers.Add(receiver);
                await db.SaveChangesAsync();
                return RedirectToAction("Create", "OrganNeededs");
            }

            ViewBag.BloodtypeID = new SelectList(db.BloodTypes, "BloodTypeID", "Description", receiver.BloodtypeID);
            return View(receiver);
        }

        // GET: Receivers/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Receiver receiver = await db.Receivers.FindAsync(id);
            if (receiver == null)
            {
                return HttpNotFound();
            }
            ViewBag.BloodtypeID = new SelectList(db.BloodTypes, "BloodTypeID", "Description", receiver.BloodtypeID);
            return View(receiver);
        }

        // POST: Receivers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDPerson,InDate,adress,BornDate,cc,Nationality,IMC,WorkerID,Name,BloodtypeID")] Receiver receiver)
        {
            if (ModelState.IsValid)
            {
                db.Entry(receiver).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.BloodtypeID = new SelectList(db.BloodTypes, "BloodTypeID", "Description", receiver.BloodtypeID);
            return View(receiver);
        }

        // GET: Receivers/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Receiver receiver = await db.Receivers.FindAsync(id);
            if (receiver == null)
            {
                return HttpNotFound();
            }
            return View(receiver);
        }

        // POST: Receivers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Receiver receiver = await db.Receivers.FindAsync(id);
            db.Receivers.Remove(receiver);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
