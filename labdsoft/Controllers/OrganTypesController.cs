﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using labdsoft.Models;

namespace labdsoft.Controllers
{
    public class OrganTypesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: OrganTypes
        public async Task<ActionResult> Index()
        {
            return View(await db.OrganTypes.ToListAsync());
        }

        // GET: OrganTypes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganType organType = await db.OrganTypes.FindAsync(id);
            if (organType == null)
            {
                return HttpNotFound();
            }
            return View(organType);
        }

        // GET: OrganTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: OrganTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "OrganTypeId,Description")] OrganType organType)
        {
            if (ModelState.IsValid)
            {
                db.OrganTypes.Add(organType);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(organType);
        }

        // GET: OrganTypes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganType organType = await db.OrganTypes.FindAsync(id);
            if (organType == null)
            {
                return HttpNotFound();
            }
            return View(organType);
        }

        // POST: OrganTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "OrganTypeId,Description")] OrganType organType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(organType).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(organType);
        }

        // GET: OrganTypes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganType organType = await db.OrganTypes.FindAsync(id);
            if (organType == null)
            {
                return HttpNotFound();
            }
            return View(organType);
        }

        // POST: OrganTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            OrganType organType = await db.OrganTypes.FindAsync(id);
            db.OrganTypes.Remove(organType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
