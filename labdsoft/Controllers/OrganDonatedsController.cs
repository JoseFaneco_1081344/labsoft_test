﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using labdsoft.Models;

namespace labdsoft.Controllers
{
    [Authorize (Roles = "PickTec, Doctor, Nurse")]
    public class OrganDonatedsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: OrganDonateds
        public async Task<ActionResult> Index()
        {
            var organDonateds = db.OrganDonateds.Include(o => o.OrganType).Include(o => o.Person);
            
            return View(await organDonateds.ToListAsync());
        }

        // GET: OrganDonateds/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganDonated organDonated = await db.OrganDonateds.FindAsync(id);
            if (organDonated == null)
            {
                return HttpNotFound();
            }
            return View(organDonated);
        }

        // GET: OrganDonateds/Create
        public ActionResult Create()
        {
            ViewBag.OrganID = new SelectList(db.OrganTypes, "OrganTypeId", "Description");
            ViewBag.IDPerson = new SelectList(db.Donors, "IDPerson", "Name");
            return View();
        }

        // POST: OrganDonateds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "OrganDonatedId,OrganID,IDPerson")] OrganDonated organDonated)
        {
            if (ModelState.IsValid)
            {
                db.OrganDonateds.Add(organDonated);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.OrganID = new SelectList(db.OrganTypes, "OrganTypeId", "Description", organDonated.OrganID);
            ViewBag.IDPerson = new SelectList(db.Donors, "IDPerson", "Name", organDonated.IDPerson);
            return View(organDonated);
        }

        // GET: OrganDonateds/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganDonated organDonated = await db.OrganDonateds.FindAsync(id);
            if (organDonated == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrganID = new SelectList(db.OrganTypes, "OrganTypeId", "Description", organDonated.OrganID);
            ViewBag.IDPerson = new SelectList(db.Donors, "IDPerson", "Name", organDonated.IDPerson);
            return View(organDonated);
        }

        // POST: OrganDonateds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "OrganDonatedId,OrganID,IDPerson")] OrganDonated organDonated)
        {
            if (ModelState.IsValid)
            {
                db.Entry(organDonated).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.OrganID = new SelectList(db.OrganTypes, "OrganTypeId", "Description", organDonated.OrganID);
            ViewBag.IDPerson = new SelectList(db.Donors, "IDPerson", "Name", organDonated.IDPerson);
            return View(organDonated);
        }

        // GET: OrganDonateds/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganDonated organDonated = await db.OrganDonateds.FindAsync(id);
            if (organDonated == null)
            {
                return HttpNotFound();
            }
            return View(organDonated);
        }

        // POST: OrganDonateds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            OrganDonated organDonated = await db.OrganDonateds.FindAsync(id);
            db.OrganDonateds.Remove(organDonated);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
