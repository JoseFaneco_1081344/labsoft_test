﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using labdsoft.Models;

namespace labdsoft.Controllers
{
    [Authorize]
    public class WasteOrganRegsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: WasteOrganRegs
        public async Task<ActionResult> Index()
        {
            var wasteOrganRegs = db.WasteOrganRegs.Include(w => w.IDOrgan);
            return View(await wasteOrganRegs.ToListAsync());
        }

        // GET: WasteOrganRegs/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WasteOrganReg wasteOrganReg = await db.WasteOrganRegs.FindAsync(id);
            if (wasteOrganReg == null)
            {
                return HttpNotFound();
            }
            return View(wasteOrganReg);
        }

        // GET: WasteOrganRegs/Create
        public ActionResult Create()
        {
            ViewBag.OrganID = new SelectList(db.OrganDonateds, "OrganDonatedId", "OrganDonatedId");
            return View();
        }

        // POST: WasteOrganRegs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "OrganID,Reason")] WasteOrganReg wasteOrganReg)
        {
            if (ModelState.IsValid)
            {
                db.WasteOrganRegs.Add(wasteOrganReg);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.OrganID = new SelectList(db.OrganDonateds, "OrganDonatedId", "OrganDonatedId", wasteOrganReg.OrganID);
            return View(wasteOrganReg);
        }

        // GET: WasteOrganRegs/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WasteOrganReg wasteOrganReg = await db.WasteOrganRegs.FindAsync(id);
            if (wasteOrganReg == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrganID = new SelectList(db.OrganDonateds, "OrganDonatedId", "OrganDonatedId", wasteOrganReg.OrganID);
            return View(wasteOrganReg);
        }

        // POST: WasteOrganRegs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "OrganID,Reason")] WasteOrganReg wasteOrganReg)
        {
            if (ModelState.IsValid)
            {
                db.Entry(wasteOrganReg).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.OrganID = new SelectList(db.OrganDonateds, "OrganDonatedId", "OrganDonatedId", wasteOrganReg.OrganID);
            return View(wasteOrganReg);
        }

        // GET: WasteOrganRegs/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WasteOrganReg wasteOrganReg = await db.WasteOrganRegs.FindAsync(id);
            if (wasteOrganReg == null)
            {
                return HttpNotFound();
            }
            return View(wasteOrganReg);
        }

        // POST: WasteOrganRegs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            WasteOrganReg wasteOrganReg = await db.WasteOrganRegs.FindAsync(id);
            db.WasteOrganRegs.Remove(wasteOrganReg);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
